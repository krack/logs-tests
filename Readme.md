# Properties in spring-boot in java.

## Global Principles
External configuration overing in spring-boot : 
https://docs.spring.io/spring-boot/docs/1.5.10.RELEASE/reference/html/boot-features-external-config.html


## Interest point : 
- src/main/resources/application.properties : in jar application.properties file.
- src/main/resources/application-dev.properties : in jar application.properties file for dev profile
- config/application.properties : external application.properties file.
- .gitpod.Dockerfile : environnement variable
- .gitpod.yml : command line variable
- .spring-boot-devtools.properties: file using for configuration (it's a copy)


## testing
- port 8080 : using jar and external application file.
- port 8081 : using code and devtool file.