package com.soprasteria.properiteslab;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import lombok.extern.java.Log;

@Log
@RestController
public class PropertiesController {

    @GetMapping("/")
    private Flux<String> log() {
        log.info("Je te dis que je log");

        return Flux.just("Je log mec");
    }
}